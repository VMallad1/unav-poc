
class Footer extends HTMLElement {
    clientId ;
    slimFooter;
    brand;
    hiderPrimaryFooter
  
    constructor() {
      super();
    }
  
    connectedCallback() {
      this.clientId = this.getAttribute("client-id") || 'default';
      this.brand = this.getAttribute("brand")|| 'tmo';
      this.slimHeader = this.getAttribute("slim-footer");
      this.hiderPrimaryFooter = this.getAttribute("hide-primary-footer");

      // this.attachShadow({ mode: "open" });
      this.render();
    }
  
    attributeChangedCallback(name, oldValue, newValue) {
      //TODO: handle dynamic updates to fields 
  
  }
  async render() {
    const html = await fetch('https://staging.t-mobile.com/content/experience-fragments/digx/tmobile/us/en/unav/xftmo-default-footer/master.html').then(res => res.text())
    this.innerHTML =html;

    this.loadScripts()
    }
    disconnectedCallback() {
  
    }

    loadScripts(){
      const s=  this.querySelectorAll('script')
      for (var i = 0; i < s.length ; i++) {
      const node=s[i], d = document.createElement('script');
      d.async=node.async;
      d.src=node.src;
      this.append(d);
    }
  }
  }
  
  customElements.define("tmo-digital-unav-footer", Footer);