
class Header extends HTMLElement {
  clientId ;
  theme ;
  slimHeader;
  brand;
  skinny;
  sticky;

  constructor() {
    super();
  }

  connectedCallback() {
    this.clientId = this.getAttribute("client-id") || 'default';
    this.theme = this.getAttribute("theme") || 'light';
    this.brand = this.getAttribute("brand")|| 'tmo';
    this.slimHeader = this.getAttribute("slim-header");
    this.skinny = this.getAttribute('skinny');
    this.sticky = this.getAttribute('sticky');
    this.attachShadow({ mode: "open" });
    this.render();
  }

  attributeChangedCallback(name, oldValue, newValue) {

}
async render() {
  const html = await fetch('https://www.t-mobile.com/content/experience-fragments/digx/tmobile/us/en/unav/xftmo-default-header/master.html').then(res => res.text())

    const domElement =new DOMParser().parseFromString(html, 'text/html')
    // this.loadScripts(domElement);

    this.shadowRoot.appendChild(domElement.head);
    this.shadowRoot.appendChild(domElement.body);

      

  }
  disconnectedCallback() {

  }
  loadScripts(domElement){
    
  var s=  domElement.querySelectorAll('script')
  for (var i = 0; i < s.length ; i++) {
  var node=s[i], parent=node.parentElement, d = document.createElement('script');
  d.async=node.async;
  d.src=`https://staging.t-mobile.com${node.src.replace(location.origin,'')}`;
  d.defer=node.defer;
  // this.append(d);
  document.head.appendChild(d)
}}


}

customElements.define("tmo-digital-unav-header", Header);